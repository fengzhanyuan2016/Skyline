﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Skyline.Core.Common
{
   public static class StringExtensions
    {

        /// <summary>
        /// TrimStart
        /// </summary>
        /// <param name="source"></param>
        /// <param name="trim"></param>
        /// <param name="stringComparison"></param>
        /// <returns></returns>
        public static string TrimStart(this string source, string trim, StringComparison stringComparison = StringComparison.Ordinal)
        {
            if (source == null)
            {
                return null;
            }
            string s = source;
            while (s.StartsWith(trim, stringComparison))
            {
                s = s.Substring(trim.Length);
            }
            return s;
        }

    }
}
