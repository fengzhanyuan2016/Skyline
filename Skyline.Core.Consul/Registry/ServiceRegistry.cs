﻿using Skyline.Core.Consul.Manager;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Skyline.Core.Consul.Registry
{
    public class ServiceRegistry : IManageServiceInstances, IManageHealthChecks
    {
        private IRegistryHost _host;
        public ServiceRegistry(IRegistryHost registryHost)
        {
            _host = registryHost;
        }

        public async Task<bool> DeregisterHealthCheckAsync(string checkId)
        {
            return await _host.DeregisterHealthCheckAsync(checkId);
        }

        public async Task<bool> DeregisterServiceAsync(string serviceId)
        {
            return await _host.DeregisterServiceAsync(serviceId);
        }

        public async Task<string> RegisterHealthCheckAsync(string serviceName, string serviceId, Uri checkUri, TimeSpan? interval = null, string notes = null)
        {
            return await _host.RegisterHealthCheckAsync(serviceName, serviceId, checkUri, interval, notes);
        }

        public async Task<RegistryInformation> RegisterServiceAsync(string serviceName, string version, Uri uri, Uri healthCheckUri = null, IEnumerable<string> tags = null)
        {
            return await _host.RegisterServiceAsync(serviceName, version, uri, healthCheckUri, tags);
        }
    }
}
