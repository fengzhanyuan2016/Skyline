﻿using Skyline.Core.Consul.Registry;
using System;
using System.Collections.Generic;
using System.Text;

namespace Skyline.Core.Consul.Discovery
{
    public class ConsulServiceDiscoveryOption
    {
        public string ServiceName { get; set; }

        public string Version { get; set; }

        public RegistryHostConfiguration Consul { get; set; }

        public string HealthCheckTemplate { get; set; }

        public string[] Endpoints { get; set; }
    }
}
