﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Skyline.Core.Consul.Discovery
{
    public class SubscriberOptions
    {
        public static readonly SubscriberOptions Default = new SubscriberOptions();

        public List<string> Tags { get; set; }

        public bool PassingOnly { get; set; } = true;
    }
}
