﻿using Skyline.Core.Consul.Manager;
using Skyline.Core.Consul.Registry;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Skyline.Core.Consul.Discovery
{
   public class ServiceDiscovery: IResolveServiceInstances
    {
        private IRegistryHost _registryHost;
        public ServiceDiscovery(IRegistryHost registryHost)
        {
            _registryHost = registryHost;
        }

        public async Task<IList<RegistryInformation>> FindAllServicesAsync()
        {
            var services=await _registryHost.FindAllServicesAsync();
            return services;
        }

        public async Task<IList<RegistryInformation>> FindServiceInstancesAsync()
        {
            var services = await _registryHost.FindServiceInstancesAsync();
            return services;
        }

        public async Task<IList<RegistryInformation>> FindServiceInstancesAsync(string name)
        {
            var services = await _registryHost.FindServiceInstancesAsync(name);
            return services;
        }

        public async Task<IList<RegistryInformation>> FindServiceInstancesAsync(Predicate<KeyValuePair<string, string[]>> nameTagsPredicate, Predicate<RegistryInformation> registryInformationPredicate)
        {
            var services = await _registryHost.FindServiceInstancesAsync(nameTagsPredicate, registryInformationPredicate);
            return services;
        }

        public async Task<IList<RegistryInformation>> FindServiceInstancesAsync(Predicate<KeyValuePair<string, string[]>> predicate)
        {
            var services = await _registryHost.FindServiceInstancesAsync( predicate);
            return services;
        }

        public async Task<IList<RegistryInformation>> FindServiceInstancesAsync(Predicate<RegistryInformation> predicate)
        {
            var services = await _registryHost.FindServiceInstancesAsync(predicate);
            return services;
        }

        public async Task<IList<RegistryInformation>> FindServiceInstancesWithVersionAsync(string name, string version)
        {
            var services = await _registryHost.FindServiceInstancesWithVersionAsync(name, version);
            return services;
        }
    }
}
